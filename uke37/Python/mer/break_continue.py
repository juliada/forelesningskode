import random

tall = random.randint(0,100)
ganger = 0 # Teller hvor mange ganger man gjetter

while True:
    gjett = int(input("Gjett et tall mellom 0 og 100: "))
    if not (0 < gjett < 100):
        print("Tallet må være mellom 0 og 100")
        continue
    ganger += 1
    if tall == gjett:
        print("Riktig! Forsøk: ",ganger)
        break
    elif gjett < tall:
        print("Høyere")
    elif gjett > tall:
        print("Lavere")


        