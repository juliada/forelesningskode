alder = int(input("Hvor gammel er du? "))
while (alder<0 or alder>120):
    print("Feil: Alder må være mellom 0 og 120!")
    alder = int(input("Hvor gammel er du? "))

print("Takk for den din "+str(alder)+"åring!!!")
# Merk bruken av + i print, den må ha str(tall)
# alternativt
print(f"Takk for den din {alder}åring!!!")


