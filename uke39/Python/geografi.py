import random

land = ['Norge','Sverige','Spania','USA','Finland','Island',
    'Danmark','Tyskland','Frankrike']

hovedsteder = ['Oslo','Stockholm','Madrid','Washington, D.C.',
           'Helsinki','Reykjavik','København','Berlin','Paris']


# skriv(land, hovedsteder)
# skrive ut, for alle par, 'Hovedstaden i Norge er Oslo.'

def skriv(land, hovedsteder):
    for i in range(len(land)):
        print(f'Hovedstaden in {land[i]} er {hovedsteder[i]}.')
        

def nytt_land(land, hovedsteder):
    tmpland = input("Land: ")
    tmpstad = input("Stad: ")
    land.append(tmpland)
    hovedsteder.append(tmpstad)
    return land, hovedsteder
    
def spm(land, hovedsteder):
    
    i = random.randint(0,len(hovedsteder))    
    svar = input(f'Hva er hovedstaden i {land[i]}')
    if svar == hovedsteder[i]:
        print("jippi")
    else:
        print("nah")

'''
Alternative løsninger som jeg snakket om på slutten av forelesningen.
Jeg manglet en parentes på slutten der, og tiden gikk ut, så det ble en
brå slutt. Men, her kommer de nå.

Alternativ 1: random.choice
'''
def spm2(land, hovedsteder):
    
    
    i, land = random.choice(enumerate(land))
    svar = input(f'Hva er hovedstaden i {land}')
    if svar == hovedsteder[i]:
        print("jippi")
    else:
        print("nah")


#     l, h = random.choice(list(zip(land, hovedsteder)))
#skriv(land,hovedsteder)
#land, hovedsteder = nytt_land(land,hovedsteder)
#skriv(land,hovedsteder)
    
spm(land, hovedsteder)


