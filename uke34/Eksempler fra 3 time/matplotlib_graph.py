import matplotlib.pyplot as plt # Vanlig kallenavn!
import numpy as np

x = [1, 2, 3, 4, 5, 6]
y = [1, 4, 55, 23, 19, 13]

plt.plot(x,y)
plt.grid()
plt.show()