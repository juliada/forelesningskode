import matplotlib.pyplot as plt # Vanlig kallenavn!

# Pie charts
labels = ['rock', 'paper', 'scissors', 'BMFG']
sizes = [15, 10, 20, 55]
explode = (0.15, 0.1, 0.2, 0.8)

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)

plt.show()
fig1.savefig('bang.png')