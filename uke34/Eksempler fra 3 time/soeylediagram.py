# Python matplotlib pyplot Examples

import matplotlib.pyplot as plt

x = ['A', 'B', 'C', 'D', 'E']
y = [22, 9, 40, 27, 12]

plt.bar(x, y)

plt.show()